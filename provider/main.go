/**
 * @Author: 小狐
 * @Date: 2021/11/27 13:23
 * @Describe 主动去学习
 *
 */
package main

import (
	"dubbo.apache.org/dubbo-go/v3/config"
	_ "dubbo.apache.org/dubbo-go/v3/imports"
	"fmt"
	"movie/boot"
	"movie/service"
	"path/filepath"
)

func main() {
	//初始化配置
	boot.Init()
	config.SetProviderService(&service.MovieProvider{})
	if configPath, err := filepath.Abs("provider/dubbogo.yml"); err != nil {
		fmt.Println("配置文件缺少", err)
	} else {
		if err := config.Load(config.WithPath(configPath)); err != nil {
			panic(err)
		}
	}
	select {}
}
