/**
 * @Author: 小狐
 * @Date: 2021/11/27 13:23
 * @Describe 主动去学习
 *
 */
package service

import (
	"context"
	"dubbo_maoyan/api/api"
	"movie/dao"
)

type MovieProvider struct {
	api.UnimplementedMovieServer
}

func (m *MovieProvider) GetMovieList(ctx context.Context, request *api.MovieRequest) (*api.MovieResponseList, error) {

	movieList := dao.MovieDao.GetMovieList(ctx, request)
	var movieResponseList = &api.MovieResponseList{}
	for _, movie := range movieList {
		response := &api.MovieResponse{
			MovieId: movie.MovieId,
		}
		movieResponseList.MovieResponse = append(movieResponseList.MovieResponse, response)
	}
	return movieResponseList, nil
}
