/**
 * @Author: 小狐
 * @Date: 2021/11/27 12:56
 * @Describe 主动去学习
 *
 */
package model

import "database/sql"

type Movie struct {
	MovieId    int32           `gorm:"column:movie_id;primary_key" json:"movie_id"`  // 影片标识
	MovieName  string        `gorm:"column:movie_name;NOT NULL" json:"movie_name"` // 影片名
	MovieImg   string        `gorm:"column:movie_img;NOT NULL" json:"movie_img"`   // 影片海报图
	Grade      float64       `gorm:"column:grade" json:"grade"`                    // 影片评分
	CreateTime sql.NullTime  `gorm:"column:create_time" json:"create_time"`        // 创建时间
	UpdateTime sql.NullTime  `gorm:"column:update_time" json:"update_time"`        // 修改时间
	IsDelete   sql.NullInt32 `gorm:"column:is_delete;default:0" json:"is_delete"`  // 模糊删除标识,0未删除,1删除
}


type MovieUnicode struct {
	Id           int32           `gorm:"column:id;primary_key" json:"id"`                    // id
	MovieId      int32           `gorm:"column:movie_id;NOT NULL" json:"movie_id"`           // 影片id
	MovieUnicode string        `gorm:"column:movie_unicode;NOT NULL" json:"movie_unicode"` // 广电编码
	CreateTime   sql.NullTime  `gorm:"column:create_time" json:"create_time"`              // 创建时间
	UpdateTime   sql.NullTime  `gorm:"column:update_time" json:"update_time"`              // 修改时间
	IsDelete     sql.NullInt32 `gorm:"column:is_delete;default:0" json:"is_delete"`        // 模糊删除标识,0未删除,1删除
}
