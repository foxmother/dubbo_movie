/**
 * @Author: 小狐
 * @Date: 2021/11/27 13:23
 * @Describe 主动去学习
 *
 */
package main

import (
	"context"
	"dubbo.apache.org/dubbo-go/v3/config"
	_ "dubbo.apache.org/dubbo-go/v3/imports"
	"fmt"
	"log"
	"movie/api"
	"path/filepath"
)

var movieConsumer = new(api.MovieClientImpl)

func init() {
	config.SetConsumerService(movieConsumer)
}

func main() {

	if configPath, err := filepath.Abs("consumer/dubbogo.yml"); err != nil {
		log.Fatal("配置文件缺少", err)
	} else {
		if err := config.Load(config.WithPath(configPath)); err != nil {
			panic(err)
		}
	}
	request := &api.MovieRequest{
		PageNum:  0,
		PageSize: 10,
		Status:   1,
	}
	resp, err := movieConsumer.GetMovieList(context.Background(), request)

	fmt.Println("错误", err)
	fmt.Println("影片数据", resp)

}
