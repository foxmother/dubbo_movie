/**
 * @Author: 小狐
 * @Date: 2021/11/27 13:08
 * @Describe 主动去学习
 *
 */
package dao

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"sync"
)

var syncDB sync.Once

var Connection *gorm.DB

func InitDB() {
	syncDB.Do(func() {
		dsn := "root:11111111@tcp(127.0.0.1:3306)/ticket_movie?charset=utf8mb4&parseTime=True&loc=Local"
		db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
			//不使用复数形式
			NamingStrategy: schema.NamingStrategy{
				SingularTable: true,
			},
		})
		//暂时不设置连接池信息
		if err != nil {
			log.Fatal("获取数据库连接失败")
		}
		Connection = db
	})

}
