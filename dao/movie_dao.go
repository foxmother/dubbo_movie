/**
 * @Author: 小狐
 * @Date: 2021/11/27 13:47
 * @Describe 主动去学习
 *
 */
package dao

import (
	"context"
	"dubbo_maoyan/api/api"
	"movie/model"
)

type movieDao struct{}

var MovieDao = &movieDao{}

func (m *movieDao) GetMovieList(ctx context.Context, request *api.MovieRequest) []*model.Movie {

	var movieList []*model.Movie
	Connection.Where(request, "status").Offset(int(request.PageNum)).Limit(int(request.PageSize)).Find(&movieList)
	return movieList
}
